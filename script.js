//mini activity
/*
	create an arrow function called postCourse which allows us to add a new object into the array. It should receive data: id, name, description, price, isActive.

	add the new course into the array and show the following alert: "You have created <nameOfCourse>. The price is <priceOfCourse>"
	-literals

	Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		- use find()

	Create an arrow function called deleteCourse which can delete the last course object in the array.
		- pop()
*/

let course = {
	id: "0001",
	name: "ICT101",
	description: "Web Development 1",
	price: 550,
	isActive: true
}
let newArray = [];
//postCourse
const postCourse = (newCourse) => {
	//add new course into array
	newArray.push(course)
	alert(`You have created ${newCourse.description}. The price is ${newCourse.price}`);
}
postCourse(course);

//findCourse
const findCourse = newArray.find(({id}) => id === '0001');
console.log(findCourse);

//deleteCourse
const deleteCourse = newArray.pop(({id}) => id === '0001');
console.log(newArray);
